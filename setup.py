from setuptools import setup, find_packages
from os import path

here = path.abspath(path.dirname(__file__))

setup(
    name='cpapilib',
    version='0.1.5',
    description='Check Point API Wrapper',
    url='https://github.com/themadhatterz/cpapilib',
    author='Joshua (Mad) Hatter',
    author_email='jhatter@themadhatter.org',
    classifiers=[
        'Development Status :: 3 - Alpha',
        'Programming Language :: Python :: 3.6',
    ],
    keywords='CheckPoint Check_Point Check Point API Wrapper',
    packages=find_packages(exclude=['tests', 'dist']),
    install_requires=['requests >= 2.18'],
)
