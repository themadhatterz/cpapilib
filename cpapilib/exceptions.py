class cpapilibException(Exception):
    """Exception Base"""

class ClientException(cpapilibException):
    def __init__(self, message, response):
        self.message = message
        if hasattr(response, 'json'):
            self.response = response.json()
        elif hasattr(response, 'text'):
            self.response = response.text
        else:
            self.response = response

class ServerException(cpapilibException):
    def __init__(self, message, response):
        self.message = message
        if hasattr(response, 'json'):
            self.response = response.json()
        elif hasattr(response, 'text'):
            self.response = response.text
        else:
            self.response = response
