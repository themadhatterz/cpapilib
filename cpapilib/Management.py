import getpass
import json
import requests

from .exceptions import ClientException, ServerException

PLURAL_EXCEPTION = {
    'access-rulebase': 'access-rulebase',
    'gateways-and-servers': 'gateways-and-servers',
    'group-with-exclusion': 'groups-with-exclusion',
    'service-tcp': 'services-tcp',
    'service-udp': 'services-udp',
    'service-icmp': 'services-icmp',
    'service-icmp6': 'services-icmp6',
    'service-sctp': 'services-sctp',
    'service-other': 'services-other',
    'application-site-category': 'application-site-categories',
    'service-dce-rpc': 'services-dce-rpc',
    'service-rpc': 'services-rpc',
    'vpn-community-meshed': 'vpn-communities-meshed',
    'vpn-community-star': 'vpn-communities-star'
}


class Management(object):

    def __init__(self, host, user, password=None, domain=None,
                 port='443', sid=None, verify=False):
        """Creation of ManagementAPI Class object.

        :param host: Check Point Managment Server
        :param user: SmartConsole User
        :param password: SmartConsole Password
        :param sid: Check Point API Session ID
        :param port: Check Point Apache Port
        :param domain: Check Point MDS Domain(optional)
        :param verify: Requests Verify SSL
        :type port: string
        """
        self.offset = 0
        self.small_limit = 50
        self.max_limit = 500
        self.user = user
        self.password = password
        self.host = host
        self.port = port
        self.domain = domain
        self.sid = sid
        self.api_version = None
        self.verify = verify
        self.request_headers = {
            'Content-Type': 'application/json',
        }

        if not verify:
            requests.packages.urllib3.disable_warnings()

    @property
    def url(self):
        """Standard URL for all Management API Calls."""
        return 'https://{}:{}/web_api/'.format(self.host, self.port)

    def _api_call(self, command, **kwargs):
        """Backbone method for sending POST to Check Point."""
        if self.sid:
            self.request_headers.update({'X-chkp-sid': self.sid})
        try:
            response = requests.post(
                self.url + command,
                data=json.dumps(kwargs),
                headers=self.request_headers,
                timeout=(15, 300),
                verify=self.verify)
        except requests.exceptions.RequestException as e:
            return 'Connection Failure: {}'.format(type(e).__name__)
        if str(response.status_code)[0] == '2':
            return response.json()
        elif str(response.status_code)[0] == '4':
            raise ClientException('Client side error.', response)
        elif str(response.status_code)[0] == '5':
            raise ServerException('Server side error.', response)

    def login(self):
        if not self.password:
            self.password = getpass.getpass('Password >> ')
        payload = {
            'user': self.user,
            'password': self.password,
        }
        if self.domain:
            payload.update({'domain': self.domain})
        response = self._api_call('login', **payload)
        if 'sid' in response:
            self.sid = response['sid']
            self.api_version = response['api-server-version']
        return response

    def publish(self, **kwargs):
        return self._api_call('publish', **kwargs)

    def discard(self, **kwargs):
        return self._api_call('discard', **kwargs)

    def logout(self, **kwargs):
        response = self._api_call('logout', **kwargs)
        if response['message'] == 'OK':
            self.sid = None
        return response

    def keepalive(self, **kwargs):
        return self._api_call('keepalive', **kwargs)

    def export(self, **kwargs):
        return self._api_call('export', **kwargs)

    def process(self, action, **kwargs):
        return getattr(self, action, **kwargs)

    def command(self, action, cptype, **kwargs):
        return getattr(self, action)(cptype, **kwargs)

    def add(self, cptype, **kwargs):
        return self._api_call('add-{}'.format(cptype), **kwargs)

    def set(self, cptype, **kwargs):
        return self._api_call('set-{}'.format(cptype), **kwargs)

    def delete(self, cptype, **kwargs):
        return self._api_call('delete-{}'.format(cptype), **kwargs)

    def show(self, cptype, single=True, **kwargs):
        if not single and cptype in PLURAL_EXCEPTION:
            cptype = PLURAL_EXCEPTION[cptype]
        elif not single:
            cptype = '{}s'.format(cptype)
        return self._api_call('show-{}'.format(cptype), **kwargs)

    def show_all(self, cptype, **kwargs):
        self.offset = 0
        if cptype in PLURAL_EXCEPTION:
            cptype = PLURAL_EXCEPTION[cptype]
        else:
            cptype = '{}s'.format(cptype)
        resp = {'to': 0, 'total': 1}
        while resp['to'] != resp['total']:
            kwargs.update({'offset': self.offset, 'limit': self.small_limit})
            self.offset += self.small_limit
            resp = self._api_call('show-{}'.format(cptype), **kwargs)
            # Zero Objects!!!
            if 'to' not in resp:
                resp['to'] = resp['total']
            yield resp

    def whereused(self, **kwargs):
        return self._api_call('where-used', **kwargs)

    def policy(self, action, **kwargs):
        """Unique to policy verification and installation."""
        return self._api_call('{}-policy'.format(action), **kwargs)

    def run(self, task, **kwargs):
        """Unique to script and ips-update."""
        return self._api_call('run-{}'.format(task), **kwargs)

    def unlock(self, **kwargs):
        """Unique to administrators."""
        return self._api_call('unlock-administrator', **kwargs)
