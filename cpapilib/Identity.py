import getpass
import json
import requests

from .exceptions import ClientException, ServerException


class Identity(object):

    def __init__(self, host, shared_secret=None, port='443', verify=False):
        """Creation of ManagementAPI Class object.

        :param host: Check Point IA Gateway
        :param port: Check Point Apache Port
        :param shared_secret: IA Shared Secret
        :param verify: Requests Verify SSL
        :type port: string
        """
        self.host = host
        self.port = port
        self.shared_secret = shared_secret
        self.verify = verify
        self.request_headers = {
            'Content-Type': 'application/json',
        }

        if not verify:
            requests.packages.urllib3.disable_warnings()

    @property
    def url(self):
        """Standard URL for all Identity API Calls."""
        return 'https://{}:{}/_IA_API/'.format(self.host, self.port)

    def _api_call(self, command, **kwargs):
        """Backbone method for sending POST to Check Point."""
        if not self.shared_secret:
            self.shared_secret = getpass.getpass('Shared Secret >> ')
        kwargs.update({'shared-secret': self.shared_secret})
        try:
            response = requests.post(
                self.url + command,
                data=json.dumps(kwargs),
                headers=self.request_headers,
                timeout=(15, 300),
                verify=self.verify)
        except requests.exceptions.RequestException as e:
            return 'Connection Failure: {}'.format(type(e).__name__)
        if str(response.status_code)[0] == '2':
            return response.json()
        elif str(response.status_code)[0] == '4':
            raise ClientException('Client side error.', response)
        elif str(response.status_code)[0] == '5':
            raise ServerException('Server side error.', response)

    def add(self, **kwargs):
        return self._api_call('add-identity', **kwargs)

    def delete(self, **kwargs):
        return self._api_call('delete-identity', **kwargs)

    def show(self, **kwargs):
        return self._api_call('show-identity', **kwargs)
