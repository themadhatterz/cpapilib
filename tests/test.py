import unittest

import cpapilib


class Test_Management(unittest.TestCase):

    apisession = cpapilib.Management('172.25.132.200', 'admin',
                                     password='vpn123',
                                     domain='Hatter-Domain1-R8010')

    def test_A_login(self):
        response = self.apisession.login()
        self.assertIn('sid', response)

    def test_B_show_all(self):
        for batch in self.apisession.show_all('host'):
            for host in batch['objects']:
                self.assertIn('name', host)

    def test_C_add(self):
        response = self.apisession.add('host', **{'name': 'unittest', 'ipv4-address': '13.13.13.13'})
        self.assertEqual(response['name'], 'unittest')

    def test_D_set(self):
        response = self.apisession.set('host', **{'name': 'unittest', 'ipv4-address': '13.13.13.14'})
        self.assertEqual(response['ipv4-address'], '13.13.13.14')

    def test_E_show(self):
        response = self.apisession.show('host', name='unittest')
        self.assertEqual(response['name'], 'unittest')

    def test_F_publish(self):
        response = self.apisession.publish()
        self.assertIn('task-id', response)

    def test_G_delete(self):
        response = self.apisession.delete('host', name='unittest')
        self.assertEqual(response['message'], 'OK')

    def test_H_publish(self):
        response = self.apisession.publish()
        self.assertIn('task-id', response)

    def test_I_discard(self):
        response = self.apisession.discard()
        self.assertEqual(response['message'], 'OK')

    def test_J_logout(self):
        response = self.apisession.logout()
        self.assertEqual(response['message'], 'OK')


if __name__ == '__main__':
    unittest.main()
