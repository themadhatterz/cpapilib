# cpapilib
Check Point API Wrapper

#### Installation
pip install cpapilib

#### Management Example
```
import cpapilib

# Login to CMA
apisession = cpapilib.Management('1.1.1.1', 'api_user', domain='CMA_NAME')
apisession.login()

# Show Single Host
onehost = apisession.show('host', name='HOST_NAME')
print(onehost['name'])

# Show All Hosts
for batch in apisession.show_all('host'):
    for host in batch['objects']:
        print(host['name'])

# Add Host
# If you must pass key word argument with '-' in it. **{} is required.
newhost = {'name': 'new_host', 'ipv4-address': '1.2.3.4'}
try:
    apisession.add('host', **newhost)
# Exceptions for Client vs Server side errors.
except (cpapilib.ClientException, cpapilib.ServerException) as E:
	print('Client(HTTP 4xx) or Server(5xx) Error')
	print(E)

# Delete Host
# Otherwise key word is fine

apisession.delete('host', name='NEW_HOST')

# Logout
apisession.logout()
```
